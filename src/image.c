#include<image.h>

_Bool is_white(unsigned char * a){
	return a[0] == 224 && a[1] == 224 && a[2] == 224;
}

char get_char(png_bytep * rowps, int h, int w, int font_size,
		Letter * chars, int lenchar){
	int i, j;
	w-=2;
	while(--lenchar!=-1){
		_Bool torf=1;
		for(i=0;i<ASCII_HEIGHT;++i){
			for(j=0;j<ASCII_WIDTH;++j){
				if(is_white(rowps[(i+h)*font_size]+(w+j)*font_size*4)
						!= chars[lenchar].t[i][j]){
					i=ASCII_HEIGHT;
					torf=0;
					break;
				}
			}
		}
		if(torf) return chars[lenchar].letter;
	}
	return (char)0;
}

int get_num(png_bytep * rowps, int h, int * w,
		int font_size, Letter * chars, int lenchar){
	char num[10];
	int i, st;
	for(i=0;(num[i]=get_char(rowps, h, *w, font_size, chars,
					lenchar));++i) *w+=6;
	*w+=4;
	sscanf(num, "%d", &st);
	return st;
}

_Bool read_coords(char * filename, Letter * chars,
		int lenchar, Coords * coords){
	int font_size=-1;
	png_bytep * rowps;
	png_uint_32 height=-1, bytewidth=-1;
	png_structp png_ptr;
	png_infop info_ptr;
	unsigned int i;

	diff_time(NULL);

	if(open_image(filename, &png_ptr, &info_ptr) == 0)
		return 0;

	diff_time("opening image");

	height=png_get_image_height(png_ptr, info_ptr);
	bytewidth=png_get_rowbytes(png_ptr, info_ptr);

	rowps = get_image_space(height, bytewidth);

	diff_time("reallocating");

	png_read_row(png_ptr, *rowps, NULL);
	for(i=1;2*i<height;++i){
		png_read_rows(png_ptr, rowps+2*i-1, NULL, 2);
		if(is_white((rowps[2*i])+4*2*i)){
			font_size=i;
			break;
		}
	}

	if(font_size==-1) return 0;

	diff_time("calculating font_size");

	png_read_rows(png_ptr, rowps+2*font_size+1, NULL, (146+ASCII_HEIGHT)*font_size-2*font_size-1);

	diff_time("reading image data");

	int k=91;
	coords->x=get_num(rowps, 146, &k, font_size, chars, lenchar);
	coords->y=get_num(rowps, 146, &k, font_size, chars, lenchar);
	coords->z=get_num(rowps, 146, &k, font_size, chars, lenchar);

	diff_time("read the coordinates");

	png_destroy_read_struct(&png_ptr, &info_ptr, INFO_NULL);

	diff_time("destoying png_read_struct");

	return 1;
}
