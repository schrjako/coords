#include<png.h>
#include<zlib.h>
#include<stdlib.h>

#define HEADER_SIZE 8
#define BUFFER_SIZE 1<<12

typedef struct{
	char * filename;
	unsigned char header[HEADER_SIZE];
	FILE * fr;
	png_structp png_ptr;
	png_infop info_ptr;
	png_uint_32 width, height;
	int bit_depth, color_type,
			interlace_type,//	0 == NONE
			compression_type, filter_method;
	//png_bytep row_pointer;
} Image;

int main(int argc, char ** argv){
	if(argc<2){
		fprintf(stderr, "not enough arguments\n");
		return 1;
	}

	Image image={.filename=argv[1]};

	image.fr = fopen(image.filename, "rb");
	if(!image.fr){
		fprintf(stderr, "couldn't open file %s\n", image.filename);
		return 2;
	}

	if(fread(image.header, 1, HEADER_SIZE, image.fr) != HEADER_SIZE){
		fprintf(stderr, "couldn't read first %d bytes of file %s\n",
				HEADER_SIZE, image.filename);
		return 3;
	}
	if(png_sig_cmp(image.header, 0, HEADER_SIZE)){
		fprintf(stderr, "file %s isn't a png\n", image.filename);
		return 4;
	}
	
	image.png_ptr = png_create_read_struct
		(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if(!image.png_ptr){
		fprintf(stderr, "couldn't create png read struct\n");
		return 5;
	}

	image.info_ptr = png_create_info_struct(image.png_ptr);
	if(!image.info_ptr){
		png_destroy_read_struct(&image.png_ptr,
				(png_infopp)NULL, (png_infopp)NULL);
		fprintf(stderr, "couldn't create png info struct\n");
		return 6;
	}

	if(setjmp(png_jmpbuf(image.png_ptr))){
		png_destroy_read_struct(&image.png_ptr, &image.info_ptr,
				(png_infopp)NULL);
		fclose(image.fr);
		fprintf(stderr, "longjump calls\n");
		return 7;
	}

	png_init_io(image.png_ptr, image.fr);
	png_set_sig_bytes(image.png_ptr, HEADER_SIZE);
	png_set_compression_buffer_size(image.png_ptr, BUFFER_SIZE);
	png_set_crc_action(image.png_ptr, PNG_CRC_WARN_USE,
			PNG_CRC_WARN_USE);

	//	low level version
	png_read_info(image.png_ptr, image.info_ptr);//problem
	png_get_IHDR(image.png_ptr, image.info_ptr, &image.width,
			&image.height, &image.bit_depth, &image.color_type,
			&image.interlace_type, &image.compression_type,
			&image.filter_method);
	printf("width: %u, height: %u\nbit_depth: %d, color_type: %d\n"
			"interlace_type: %d, compression_type: %d\nfilter_method: %d\n",
			image.width, image.height, image.bit_depth,
			image.color_type, image.interlace_type,
			image.compression_type, image.filter_method);
	printf("%s: %d\n%s: %d\n%s: %d\n%s: %d\n%s: %d\n", 
			"PNG_COLOR_TYPE_GRAY",
			PNG_COLOR_TYPE_GRAY,
			"PNG_COLOR_TYPE_GRAY_ALPHA",
			PNG_COLOR_TYPE_GRAY_ALPHA,
			"PNG_COLOR_TYPE_PALETTE",
			PNG_COLOR_TYPE_PALETTE,
			"PNG_COLOR_TYPE_RGB",
			PNG_COLOR_TYPE_RGB,
			"PNG_COLOR_TYPE_RGB_ALPHA",
			PNG_COLOR_TYPE_RGB_ALPHA);
	printf("rowbytes: %lu\n", png_get_rowbytes(image.png_ptr, image.info_ptr));
	//image.row_pointer =
	//	malloc(png_get_rowbytes(image.png_ptr, image.info_ptr));
	//if(image.row_pointer == NULL){
	//	fprintf(stderr, "rowbyte malloc failed\n");
	//	return 8;
	//}
	unsigned int i, j;
	png_bytep * row_pointers = malloc(sizeof(png_bytep)*image.height);
	row_pointers[0]=malloc(png_get_rowbytes(image.png_ptr, image.info_ptr)*image.height);
	for(j=1;j<image.height;++j) row_pointers[j]=row_pointers[j-1]+png_get_rowbytes(
				image.png_ptr, image.info_ptr);
	//for(j=0;j<image.height;++j) row_pointers[j]=malloc(png_get_rowbytes(
				//image.png_ptr, image.info_ptr));
	//png_read_image(image.png_ptr, row_pointers);
	const int size=600+6*3+10*3;
	gzseek(image.fr, size*png_get_rowbytes(image.png_ptr, image.info_ptr), SEEK_CUR);
	png_read_rows(image.png_ptr, row_pointers, NULL, image.height-size);
	for(j=0;j<image.height-size;j+=3){
		//png_read_row(image.png_ptr, image.row_pointer, NULL);
		//printf("row: %d\n", j);
		for(i=0;i<image.width;i+=3){
			putchar((row_pointers[j][4*i+0]==224 &&
						row_pointers[j][4*i+0]==row_pointers[j][4*i+2] &&
						row_pointers[j][4*i+1]==row_pointers[j][4*i+2])?'#':' ');
		}
		putchar(10);
	}
	return 0;
}
