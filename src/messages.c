#include<messages.h>

void handle_error(char * format_string, ...){
	va_list args;
	va_start(args, format_string);
	fprintf(stderr, COLOR_RED);
	if(errno) fprintf(stderr, "ERROR: %s\n\t", strerror(errno));
	vfprintf(stderr, format_string, args);
	fprintf(stderr, "\n" COLOR_WHITE);
}
