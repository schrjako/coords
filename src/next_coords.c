#include<next_coords.h>

Coords curr_pos;

int comp(const void * a, const void * b){
	if( (((Coords*)b)->visited&curr_pos.visited) -
			(((Coords*)a)->visited&curr_pos.visited) )
		return (((Coords*)b)->visited&curr_pos.visited) -
						(((Coords*)a)->visited&curr_pos.visited);
	return coord_dist(b, &curr_pos) - coord_dist(a, &curr_pos);
}

int main(int argv, char ** argc){
	int user, pointnum=0;
	sscanf(argc[1], "%d", &user);
	curr_pos.visited=1<<user;
	if(sscanf(argc[2], "%d,%d,%d", &curr_pos.x,
				&curr_pos.y, &curr_pos.z)!=3){
		Letter * chars;
		int lenchar=get_letters(&chars, "data/ascii_numbs.txt");
		if(read_coords(argc[2], chars, lenchar, &curr_pos)==0){
			handle_error("couldn't determine your position");
			return 1;
		}
	}
	printf("curr_pos: ");
	print_coords(&curr_pos, &stdout);
	Coords *points;
	pointnum=get_coords(&points, argc[3]);
	qsort(points, pointnum, sizeof(Coords), comp);
	put_coords(points, &stdout, pointnum);
	return 0;
}
