#include<coord_utils.h>

int scan_coords(Coords * c, FILE ** fr){
	return fscanf(*fr, " x: %d, y: %d, z: %d, visited: %lu",
			&c->x, &c->y, &c->z, &c->visited);
}

int print_coords(Coords * c, FILE ** fw){
	return fprintf(*fw, "x: %6d, y: %6d, z: %6d, visited: %6lu\n",
			c->x, c->y, c->z, c->visited);
}

#define sq(a) ((a)*(a))
int coord_dist(const Coords * a, const Coords * b){
	return sq(a->x-b->x)+sq(a->y-b->y)+sq(a->z-b->z);
}
#undef sq

int comp_coords(const void * a, const void * b){
	if( ((Coords*)a)->x - ((Coords*)b)->x )
		return ((Coords*)a)->x - ((Coords*)b)->x; 
	if( ((Coords*)a)->y - ((Coords*)b)->y )
		return ((Coords*)a)->y - ((Coords*)b)->y; 
	return ((Coords*)a)->z - ((Coords*)b)->z; 
}

void put_coords(Coords * coords, FILE ** fw, int size){
	while(size--) print_coords(coords+size, fw);
}

int get_coords(Coords ** coords, const char * in){
	FILE * fr = open_file(in, "r");
	if(fr==NULL) return 0;
	int size=10, pos=0;
	*coords=malloc(size * sizeof(Coords));
	while(scan_coords(*coords+pos, &fr) == 4){
		++pos;
		if(pos==size) *coords=realloc(*coords, sizeof(Coords) * (size*=2));
	}
	fclose(fr);
	return pos;
}
