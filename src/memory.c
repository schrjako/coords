#include<memory.h>

png_bytep * get_image_space(png_uint_32 h, png_uint_32 w){
	static png_uint_32 width=0, height=0;
	static png_bytep * rowps=NULL;
	png_uint_32 i;
	if(w==width && h==height) return rowps;
	if(h==0){
		puts("freeing");
		for(i=0;i<height;++i) free(rowps[i]);
		free(rowps);
		rowps=NULL;
	}
	else{
		if(h<height) for(i=h;i<height;++i) free(rowps[i]);
		rowps = realloc(rowps, h * sizeof(png_bytep));
		if(height<h) memset(rowps+height, 0, (h-height)*sizeof(png_bytep));
		for(i=0;i<h;++i) rowps[i] = realloc(rowps[i], w);
	}
	width=w;
	height=h;
	return rowps;
}
