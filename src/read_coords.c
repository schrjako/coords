#include<read_coords.h>

int main(int argc, char ** argv){
	Letter * chars;
	int lenchar=get_letters(&chars, "data/ascii_numbs.txt");
	errno=0;
	if(argc<3){
		handle_error("not enough arguments");
		return 1;
	}
	char * filename;
	char full_filename[500];
	filename=next_filename(argv[1]);
	Coords ans;
	sscanf(argv[2], "%lu", &ans.visited);
	ans.visited=1<<ans.visited;
	while(filename!=NULL){
		sprintf(full_filename, "%s/%s", argv[1], filename);
		if(read_coords(full_filename, chars, lenchar, &ans))
			print_coords(&ans, &stdout);
		filename=next_filename(NULL);
	}
	return 0;
}
