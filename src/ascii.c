#include<ascii.h>

_Bool read_letter(Letter * l, FILE ** fr){
	if((l->letter=fgetc(*fr))==EOF) return 0;
	for(int i=0;i<ASCII_HEIGHT;++i){
		fgetc(*fr);
		for(int j=0;j<ASCII_WIDTH;++j)
			l->t[i][j]=fgetc(*fr)=='#'?1:0;
	}
	fgetc(*fr);
	return 1;
}

int get_letters(Letter ** letters, const char * in){
	FILE * fr = open_file(in, "r");
	if(fr==NULL) return 0;
	int size=10, pos=0;
	*letters=malloc(size * sizeof(Letter));
	while(read_letter(*letters+pos, &fr)){
		++pos;
		if(pos==size) *letters=realloc(*letters, sizeof(Letter) * (size*=2));
	}
	fclose(fr);
	return pos;
}

void print_letter(Letter * l, FILE ** fw){
	fprintf(*fw, "%c\n", l->letter);
	for(int i=0;i<ASCII_HEIGHT;++i){
		for(int j=0;j<ASCII_WIDTH;++j) fputc(l->t[i][j]?'#':' ', *fw);
		fputc(10, *fw);
	}
}
