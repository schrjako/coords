#include<measure.h>

void diff_time(char * action){
	static clock_t prev;
	clock_t curr=clock();
#ifdef PRINT_TIME
	if(action!=NULL)
		printf("processor time for %s: %ld\n", action, curr-prev);
#endif
	prev=curr;
}
