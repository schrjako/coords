#include<png_utils.h>

_Bool open_image(char * filename,
		png_structp * png_pptr, png_infop * info_pptr){

	unsigned char header[HEADER_SIZE];
	FILE * fr;
	errno=0;
	fr=fopen(filename, "rb");
	if(fr == NULL){
		handle_error("opening file %s for binary reading",
				filename);
		return 0;
	}

	if(fread(header, 1, HEADER_SIZE, fr) != HEADER_SIZE){
		handle_error("reading first %d bytes of %s", HEADER_SIZE,
				filename);
		fclose(fr);
		return 0;
	}

	if(png_sig_cmp(header, 0, HEADER_SIZE)){
		handle_error("File %s isn't a image of type png", filename);
		fclose(fr);
		return 0;
	}

	*png_pptr=png_create_read_struct (PNG_LIBPNG_VER_STRING,
			NULL, NULL, NULL);
	if(*png_pptr == NULL){
		handle_error("couldn't create png_read_struct");
		fclose(fr);
		return 0;
	}

	*info_pptr=png_create_info_struct(*png_pptr);
	if(*info_pptr==NULL){
		handle_error("couldn't create png_info_struct");
		png_destroy_read_struct(png_pptr, INFO_NULL, INFO_NULL);
		fclose(fr);
		return 0;
	}

	if(setjmp(png_jmpbuf(*png_pptr))){
		png_destroy_read_struct(png_pptr, info_pptr, INFO_NULL);
		fclose(fr);
		handle_error("longjump called");
		return 0;
	}

	png_init_io(*png_pptr, fr);
	png_set_sig_bytes(*png_pptr, HEADER_SIZE);
	png_set_compression_buffer_size(*png_pptr, BUFFER_SIZE);
	png_set_crc_action(*png_pptr, PNG_CRC_WARN_USE,
			PNG_CRC_WARN_USE);
	png_read_info(*png_pptr, *info_pptr);
	return 1;
}
