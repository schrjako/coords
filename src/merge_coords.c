#include<merge_coords.h>

int main(int argv, char ** argc){
	FILE * fw=fopen(argc[1], "w");
	Coords *in, *merged=NULL;
	int curr, size=0, i;
	for(i=2;i<argv;++i){
		curr=get_coords(&in, argc[i]);
		merged=realloc(merged, sizeof(Coords) * (size+curr));
		memcpy(merged+size, in, sizeof(Coords) * curr);
		free(in);
		size+=curr;
	}
	qsort(merged, size, sizeof(Coords), comp_coords);
	for(curr=1,i=1;i<size;++i){
		if(comp_coords(merged+i, merged+i-curr)==0){
			merged[i].visited|=merged[i-curr].visited;
			++curr;
		}
		merged[i-curr+1]=merged[i];
	}
	size-=curr-1;
	put_coords(merged, &fw, size);
	return 0;
}
