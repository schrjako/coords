#include<files.h>

FILE * open_file(const char * filename, const char * mode){
	FILE * f = fopen(filename, mode);
	if(f == NULL){
		char format[100]="couldn't open file %s for";
		if(mode[1]=='b') strcat(format, " binary");
		strcat(format, mode[0]=='r'?" reading":" writing");
		handle_error(format, filename);
	}
	return f;
}

char * next_filename(char * dirname){
	static struct dirent * file;
	static DIR * directory=NULL;
	if(dirname != NULL) directory=opendir(dirname);
	if(directory == NULL){
		handle_error("when opening directory: %s", dirname);
		return NULL;
	}
	while((file = readdir(directory)) != NULL)
		if(file->d_type == 8 && strcmp(".png",
					file->d_name+strlen(file->d_name)-4) == 0)
			return file->d_name;
	return 0;
}
