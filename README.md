Installation process:

If you already are on Linux or have a Linux distro installed in WSL you can skip to point 7.
1. Open PowerShell as Administrator and run:
	Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
2. Restart the computer when prompted.
3. Go to the Microsoft Store.
4. Search for Linux.
5. Choose and install a Linux distro (I am testing everything on debian so that is the prefered choice).
6. When installed launch it as administrator and put in a new username and password as prompted.
7. Execute the following commands in the command line (rightclick in Linux emulator on Windows to paste,
		on linux it is usually ctrl+shift+v):

> sudo apt install make git libpng-dev libncurses-dev

> git clone https://gitlab.com/schrjako/coords

> cd coords

> make

8. You (probably) made it! The executable binary is in the directory bin and is named coords.

Updating:
To update simply open the terminal and run these commands:

> cd coords

> git pull

> make

Program usage:

In this version you have to run the command from the home directory of the project (named coords).
It takes 1 argument which should be the path to a directory with minecraft screenshots. It then tries
to read the coordinates from the images. On success it writes them to standard output in the format
x, y, z coordinates, if the image does not contain coordinates it ignores it, if an error occurs it
reports it and moves on to the next image.
