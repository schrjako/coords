W = -Wall -Wextra -Wshadow -pedantic
libs = png
libs := $(patsubst %,-l%,$(libs))
all: bin/read_coords bin/test bin/merge_coords bin/next_coords
objdir = build
bindir = bin
srcdir = src
incldir = include
depdir = dependencies
cc = gcc $(incldir:%=-I%) 
flags = $W -o $@

readobjs = read_coords.o ascii.o messages.o image.o files.o memory.o measure.o png_utils.o coord_utils.o
fullname_readobjs = $(patsubst %,$(objdir)/%,$(readobjs))

mergeobjs = messages.o files.o coord_utils.o merge_coords.o
fullname_mergeobjs = $(patsubst %,$(objdir)/%,$(mergeobjs))

nextobjs = messages.o image.o ascii.o files.o coord_utils.o next_coords.o png_utils.o measure.o memory.o
fullname_nextobjs = $(patsubst %,$(objdir)/%,$(nextobjs))

objs = $(sort $(readobjs) $(mergeobjs) $(nextobjs))

.PHONY: clean

ifeq ($(option),print_measurement)
flags+= -DPRINT_TIME
endif

bin/next_coords: $(fullname_nextobjs) | $(bindir)
	$(cc) $(flags) $^ $(libs)

bin/merge_coords: $(fullname_mergeobjs) | $(bindir)
	$(cc) $(flags) $^ $(libs)

bin/read_coords: $(fullname_readobjs) | $(bindir)
	$(cc) $(flags) $^ $(libs)

$(objdir)/%.o: $(srcdir)/%.c $(depdir)/%.d | $(objdir) $(depdir)
	$(cc) $(flags) -c $<

$(depdir)/%.d: $(srcdir)/%.c | $(depdir)
	@set -e; rm -f $@; \
	$(cc) -M $< | sed 's,\($*\)\.o[ :]*,$(objdir)/\1.o $@ : ,g' > $@

include $(patsubst %,$(depdir)/%,$(objs:%.o=%.d))

$(objdir) $(bindir) $(depdir):
	mkdir $@

bin/test: src/test.c | $(bindir)
	$(cc) $(flags) src/test.c $(libs) -lz

clean:
	rm $(objdir) $(bindir) $(depdir) -fr
