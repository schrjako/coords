#ifndef MESSAGES
#define MESSAGES

#include<stdio.h>
#include<errno.h>
#include<stdarg.h>
#include<string.h>

#define COLOR_RED "\x1b[31m"
#define COLOR_GREEN "\x1b[32m"
#define COLOR_WHITE "\x1b[0m"

/*
 * Prints a custom error message to stderr. The message consists
 * of the error message strerror(errno) returns (only if errno is
 * not 0) and caller-defined output (works just like printf).
 */
void handle_error(char * format_string, ...);

#endif//MESSAGES
