#ifndef FILES
#define FILES

#include<string.h>
#include<dirent.h>
#include<errno.h>
#include<stdio.h>

#include<messages.h>

#define FILENAME_LEN 500

/*
 * Returns the next filename of d_type == 8 and ending with .png.
 * 
 * On the first call to next_filename(), the name of the directory
 * with the png-images should be specified in dirname. In each subsequent
 * call that should return the next filename from the same directory,
 * dirname must be NULL.
 */ 
char * next_filename(char * dirname);

/*
 * Works like fopen but reports an error if it occurs.
 */
FILE * open_file(const char * filename, const char * mode);

#endif//FILES
