#ifndef MEMORY
#define MEMORY

#include<stdlib.h>
#include<string.h>
#include<png.h>

/*
 * Returns a png_bytep array with space for a png-image of height h
 * pixels and width of w bytes.
 * If h is 0, it frees the allocated memory.
 */
png_bytep * get_image_space(png_uint_32 h, png_uint_32 w);

#endif//MEMORY
