#ifndef PNG_UTILS
#define PNG_UTILS

#include<stdio.h>
#include<png.h>
#include<errno.h>

#include<messages.h>

#define HEADER_SIZE 8
#define BUFFER_SIZE 1<<13
#define INFO_NULL (png_infopp)NULL

/*
 * Opens png image whoose name is the string filename and
 * prepares it for reading the image data. It initializes 
 * the png_read_struct and png_info_struct.
 *
 * Returns 1 on sucess and 0 on failure.
 */
_Bool open_image(char * filename,
		png_structp * png_pptr, png_infop * info_pptr);

#endif//PNG_UTILS
