#ifndef MEASURE
#define MEASURE

#include<time.h>
#include<stdarg.h>
#include<stdio.h>

/*
 * Measure runtime between different points.
 */
void diff_time(char * action);

#endif//MEASURE
