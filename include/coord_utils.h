#ifndef COORD_UTILS
#define COORD_UTILS

#include<stdio.h>
#include<stdlib.h>

#include<files.h>

typedef struct{
	int x, y, z;
	long unsigned int visited;
} Coords;

/*
 * Scans/prints coords from/to file stream fr/fw into/from
 * c and returns the fscanf/fprintf return value.
 */
int scan_coords(Coords * c, FILE ** fr);
int print_coords(Coords * c, FILE ** fw);

/*
 * Prints size Coords structs to fw.
 */
void put_coords(Coords * coords, FILE ** fw, int size);

/*
 * Gets all Coords structs from the file whoose name is in.
 * Allocates memory for storing them and you have to free it.
 */
int get_coords(Coords ** coords, const char * in);

/*
 * Returns the square of the distance between a and b.
 */
int coord_dist(const Coords * a, const Coords * b);

/*
 * Compares two structs Coords after their x, y and z
 * values.
 */
int comp_coords(const void * a, const void * b);

#endif//COORD_UTILS
