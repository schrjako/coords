#ifndef ASCII
#define ASCII

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#include<files.h>

#define ASCII_WIDTH 5
#define ASCII_HEIGHT 8

typedef struct{
	char letter;	//the letter that is stored in t
	_Bool t[ASCII_HEIGHT][ASCII_WIDTH];
} Letter;

/*
 * Reads a letter from filestram pointed to by fr to Letter struct l.
 */
_Bool read_letter(Letter * l, FILE ** fr);

/*
 * Reads all the letters from file whoose name is in.
 * Allocates the memory and doesn't free it.
 */
int get_letters(Letter ** letters, const char * in);

/*
 * Prints the letter l points to to filestream fw.
 */
void print_letter(Letter * l, FILE ** fw);

#endif//ASCII
