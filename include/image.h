#ifndef IMAGE
#define IMAGE

#include<stdio.h>
#include<png.h>
#include<errno.h>
#include<string.h>
#include<stdlib.h>

#include<messages.h>
#include<files.h>
#include<ascii.h>
#include<memory.h>
#include<measure.h>
#include<png_utils.h>
#include<coord_utils.h>

/*
 * Reads the coordinates from the image whoose full (absolute or
 * relative) path is filename.
 */
_Bool read_coords(char * filename, Letter * chars,
		int lenchar, Coords * coords);

#endif//IMAGE
